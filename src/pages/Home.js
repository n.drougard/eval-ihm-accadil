import React from 'react';

const Home = () => (
    <div className="container">
      <div className="row">
        <div className="col-12 p-5 text-center">
          <h1 className="display-1 fw-bold">L’étude de monnaies grâce à ACCADIL</h1>
          <p className="display-8">
            Grâce à l’intelligence artificielle, ACCADIL vous permet de repérer les liaisons de monnaie sans y passer des heures ni vous épuiser.
          </p>
        </div>
      </div>
      <div className="text-center p-3"></div>
      <div className="row justify-content-center">
        <div className="col-md-6"></div>
      </div>
      <footer id="site-footer" className="container-fluid p-3 text-center mt-auto">
        <p>Copyright &copy;2024</p>
      </footer>
    </div>
  );
  
export default Home;